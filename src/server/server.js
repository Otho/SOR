var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var config = require('./config');

var app = express();
var router = express.Router();

var port = process.env.API_PORT || 3001;

mongoose.connect(config.database);

// now we should configure the APi to use bodyParser and look for JSON data in the body
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(require('prerender-node'));

// To prevent errors from Cross Origin Resource Sharing, we will set our headers to allow CORS with middleware like so:
app.use(function(req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Credentials', 'true');
  res.setHeader('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT,DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');

  // and remove cacheing so we get the most recent comments
  res.setHeader('Cache-Control', 'no-cache');
  next();
});

// adding the /comments route to our /api router
router.use(require('./api/user/user_router'));
router.use(require('./api/blog/post/post_router'));
router.use(require('./api/blog/comment/comment_router'));

// Use our router configuration when we call /api
app.use('/api', router);

// starts the server and listens for requests
var server = app.listen(port, function() {
  console.log(`api running on port ${port}`);
});

var io = require('socket.io')(server);
var users = [];
io.on('connection', function(socket){
  socket.on('chatconnect', function(user){
    console.log(user.username + " has connected!");
    user.socket = socket;
    users.push(user);
    io.emit('chat message', {user: '', text: "User " + user.username + " has connected!"});
    io.emit('users', users.map(function(user) {return {username: user.username}}));
  });

  socket.on('disconnect', function(){
    var result = users.findIndex(function( user ) {
      return user.socket === socket;
    });

    if ( result >= 0 ) {
      users.splice(result, 1);
      io.emit('users', users.map(function(user) {return {username: user.username}}));
    }
  });
  socket.on('chat message', function(msg){
    if (msg.text[0] === '/') {
      // message is a comand
      let command = msg.text.match(/\/d(\d+)\s*([+|-]\s*\d+)?/i);
      if ( command ){
        msg.text = "Rolled a d" + command[1] +(command[2] ? " "+ command[2] : "" )+": " + eval(Math.ceil(Math.random() * command[1]) + (command[2] ? command[2] : "" ));
      } else {
        msg.text = "Tried an unkown command";
      }
    }
    io.emit('chat message', msg);
  });
});

module.exports = app
