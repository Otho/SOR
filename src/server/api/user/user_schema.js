/**
 * File that defines what a user is
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = new Schema({
  //Required for registration
  username: String,
  email: String,
  password: String,
  
  //Can be edited later:
  roles: [String],
  nickname: String,
  preferredNarratives: String,
  preferredSystems: String,
  preferredRole: String,
  //avatar: ComoFas?,
  realName: String,
  phone: String,
  socialMedia: [String]
});

module.exports = mongoose.model('User', UserSchema);