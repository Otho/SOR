/**
 * File that defines what a blog post is
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var RoomSchema = new Schema({
  isPrivate: Boolean,
  name: String,
  description: String,

  users: [{type: mongoose.Schema.Types.ObjectId, ref: 'RoomUserSchema'}]
}, {
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  }
});

module.exports = mongoose.model('RoomSchema', RoomSchema);
