// import axios from 'axios';
// import {API} from 'config';

class Tables {

  constructor() {
    this._tables = [];
    this.fetch();
  }

  fetch(callback) {
    // let tables = this;
    this._tables = [
      {
        id: 0,
        name: "D&D 4 Ever",
        description: "The coolest D&D room from SOR",
        isPrivate: false,
        users: [{name:"Otho"}]
      }
    ]
    // axios.get(API.blog_posts, {
    //
    // })
    // .then(res => {
    //   posts._posts = res.data;
    //   if (callback) {
    //     callback(posts._posts);
    //   }
    // })
    // .catch(err => {
    //   console.error(err);
    // });
  }

  createTable(table, callback, obj) {
    // let posts = this;
    // axios.post(API.blog_posts, post)
    // .then(res => {
    //   console.log(res.data.success);
    //   let dateObj = new Date();
    //   let month = dateObj.getUTCMonth() + 1; //months from 1-12
    //   let day = dateObj.getUTCDate();
    //   let year = dateObj.getUTCFullYear();
    //   post.perma_link = `${year}_${month}_${day}_${post.title}`
    //   posts._posts.splice(0, 0, post);
    //   callback.call(obj, res.data);
    // })
    // .catch(err => {
    //   console.error(err);
    //   callback.call(obj, err);
    // });
  }

  getTable(perma_link) {
    // let post = this._posts.find(function(toFind){return toFind.perma_link === perma_link});
    // return post;
  }

  updateTable(post, callback, obj) {
    // let posts = this;
    // axios.put(API.blog_posts+'/'+post.perma_link, post)
    // .then(res => {
    //   console.log(res.data.success);
    //   let id_to_update = posts._posts.findIndex(function(toFind){return toFind.perma_link === post.perma_link});
    //   posts._posts[id_to_update] = post;
    //   callback.call(obj, res.data);
    // })
    // .catch(err => {
    //   console.error(err);
    //   callback.call(obj, err);
    // });
  }

  deleteTable(post, callback, obj) {
    // let posts = this;
    // axios.delete(API.blog_posts+'/'+post.perma_link)
    // .then(res => {
    //   console.log(res.data.success);
    //   let id_to_update = posts._posts.findIndex(function(toFind){return toFind.perma_link === post.perma_link});
    //   posts._posts.splice(id_to_update, 1);
    //   callback.call(obj, res.data);
    // })
    // .catch(err => {
    //   console.error(err);
    //   callback.call(obj, err);
    // });
  }

  get tables() {
    return this._tables;
  }
}

export default new Tables();
