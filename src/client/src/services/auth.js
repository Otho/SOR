import axios from 'axios';
import {API} from 'config';
import jwtDecode from 'jwt-decode';
import {saveUser} from 'services/persist';

import history from 'appHist';

class Auth {

  constructor() {
    this.token = undefined;
    this.username = undefined;
    this.email = undefined;
    this.roles = [];
    this.nickname = undefined;
    this.preferredSystems = undefined;
    this.preferredNarratives = undefined;
    this.preferredRole = undefined;
    this.realName = undefined;
    this.phone = undefined;
    this.socialMedia = [];
  }

  log(user) {
    this.token = user.token;
    this.username = user.username;
    this.email = user.email;
    this.roles = user.roles;
    this.nickname = user.nickname;
    this.preferredSystems = user.preferredSystems;
    this.preferredNarratives = user.preferredNarratives;
    this.preferredRole = user.preferredRole;
    this.realName = user.realName;
    this.phone = user.phone;
    this.socialMedia = user.socialMedia;
  }

  isValidToken(token) {
    if ( !token ) return false;

    let decoded = jwtDecode(token);
    return Date.now() / 1000 < decoded.exp;
  }

  login(user, password, callback) {
    let auth = this;
    axios.post(API.user, {
      username: user,
      password: password
    })
    .then(res => {
      if( !res.data.success ) return
      auth.token = res.data.token;
      auth.username = res.data.username;
      auth.roles = res.data.roles;
      auth.email = res.data.email;
      auth.nickname = res.data.nickname;
      auth.preferredSystems = res.data.preferredSystems;
      auth.preferredNarratives = res.data.preferredNarratives;
      auth.preferredRole = res.data.preferredRole;
      auth.realName = res.data.realName;
      auth.phone = res.data.phone;
      auth.socialMedia = res.data.socialMedia;

      saveUser(
        {
          token: auth.token,
          username: auth.username,
          roles: auth.roles,
          email: auth.email,
          nickname: auth.nickname,
          preferredSystems: auth.preferredSystems,
          preferredNarratives: auth.preferredNarratives,
          preferredRole: auth.preferredRole,
          realName: auth.realName,
          phone: auth.phone,
          socialMedia: auth.socialMedia
        }
      )
      callback();
    })
    .catch(err => {
      console.error(err);
    });
  }

  register(user, pass, passCheck, mail, callback) {
    axios.put(API.user, {
      username: user,
	  email: mail,
      password: pass,
	  passcheck: passCheck
    })
    .then(res => {
      callback();
    })
    .catch(err => {
      console.error(err);
    });
  }

  logout() {
    this.token = undefined;
    saveUser(
      {
        token: undefined,
        username: undefined,
        roles: undefined,
        email: undefined,
        nickname: undefined,
        preferredSystems: undefined,
        preferredNarratives: undefined,
        preferredRole: undefined,
        realName: undefined,
        phone: undefined,
        socialMedia: undefined
      }
    )

    history.push('/')
  }

  get isAuthenticated() {
    return this.token !== undefined;
  }

  is(role) {
    return this.isAuthenticated && this.roles.includes(role);
  }
}

const auth = new Auth();

export default auth;
