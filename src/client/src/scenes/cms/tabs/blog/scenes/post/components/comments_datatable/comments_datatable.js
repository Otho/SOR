import React from 'react';
import Datatable from 'components/datatable/datatable';
import Posts from 'services/posts';

export default class CommentsDataTable extends React.Component {

  constructor(props) {
    super(props);

    this.handleSelect = this.handleSelect.bind(this);
    this.handleDelete = this.handleDelete.bind(this);

    this.state = {
      selected: []
    }
  }

  handleSelect(selected) {
    this.setState({selected: selected});
  }

  handleDelete(event) {
    Posts.deleteComment(this.props.data, this.props.data.comments[this.state.selected[0]], function() {
      this.props.handleDelete();
    }, this);
  }

  render() {
    return (
      <Datatable
        columns={[
          {
            title: "Author",
            key: "author",
            component: Author
          },
          {
            title: "Text",
            key: "text",
            component: Text
          }
        ]}

        searchPlaceholder="Find a comment"
        multiSelect={true}
        data={this.props.data.comments}
        selected={this.state.selected}

        handleSelect={this.handleSelect}
        handleDelete={this.handleDelete}
      />
    )
  }

}

function Author(props) {
  return (<h1>{props.author.username}</h1>);
}

function Text(props) {
  return (<p>{props.text}</p>);
}
