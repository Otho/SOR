import React from 'react';
import Datatable from 'components/datatable/datatable';
import Posts from 'services/posts';

export default class BlogTabDatatable extends React.Component {

  constructor(props) {
    super(props);

    this.handleCreate = this.handleCreate.bind(this);
    this.handleSelect = this.handleSelect.bind(this);
    this.handleView = this.handleView.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.handleDelete = this.handleDelete.bind(this);

    this.state = {
      selected: []
    }
  }

  handleSelect(selected) {
    this.setState({selected: selected});
  }

  handleCreate(event) {
    this.props.history.push( this.props.location.pathname + '/post/new')
  }

  handleView(event) {
    this.props.history.push( this.props.location.pathname + '/post/view/' + this.props.data[this.state.selected[0]].perma_link);
  }

  handleEdit(event) {
    this.props.history.push( this.props.location.pathname + '/post/edit/' + this.props.data[this.state.selected[0]].perma_link);
  }

  handleDelete(event) {
    Posts.deletePost(this.props.data[this.state.selected[0]], function() {
      this.props.history.replace( '/cms/blog' );
    }, this);
  }

  render() {
    return (
      <Datatable
        columns={[
          {
            title: "Title",
            key: "title",
            component: Title,
            props: {
              handleClick: () => {console.log("Halo");}
            }
          },
          {
            title: "Author",
            key: "author",
            component: Author,
            props: {
              handleClick: () => {console.log("Halo");}
            }
          }
        ]}

        searchPlaceholder="Find a post"
        multiSelect={true}
        data={this.props.data}
        selected={this.state.selected}

        handleSelect={this.handleSelect}

        handleCreate={this.handleCreate}
        handleView={this.handleView}
        handleEdit={this.handleEdit}
        handleDelete={this.handleDelete}
      />
    )
  }

}

function Title(props) {
  return (<h1>{props.title}</h1>);
}

function Author(props) {
  return (<h1>{props.author}</h1>);
}
