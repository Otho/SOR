import React from 'react';
import Auth from '../../services/auth';
import './register.css';

export default class Register extends React.Component{

  constructor(props) {
    super(props);
    this.state = {
      user: '',
	    email: '',
      password: '',
	    passcheck: ''
    }

    this.handleRegister = this.handleRegister.bind(this);
    this.handleUserChange = this.handleUserChange.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handlePassCheckChange = this.handlePassCheckChange.bind(this);
  }

  handleRegister(event) {
    event.preventDefault();

    if (this.state.password === this.state.passcheck)
      Auth.register(this.state.user, this.state.password, this.state.passcheck, this.state.email, () => {
        this.props.history.push('/')
      });
    }

  handleUserChange(event) {
    this.setState({user: event.target.value});
  }

  handleEmailChange(event) {
    this.setState({email: event.target.value});
  }

  handlePasswordChange(event) {
    this.setState({password: event.target.value});
  }

  handlePassCheckChange(event) {
    this.setState({passcheck: event.target.value});
  }

  render () {
    return (
      <div className="columns">
        <div className="column is-2 is-offset-5 has-text-centered">
          <div className="register">
            <h2 className="title"> Register </h2>
            <form onSubmit={this.handleRegister} className="has-text-left">
              <div className="field">
                <label className="label">Choose an Username</label>
                <p className="control">
                  <input className="input" type="text" name="user" placeholder="Username" value={this.state.user} onChange={this.handleUserChange} />
                </p>
              </div>
			  <div className="field">
                <label className="label">E-Mail</label>
                <p className="control">
                  <input className="input" type="text" name="email" placeholder="your@email.com" value={this.state.email} onChange={this.handleEmailChange} />
                </p>
              </div>
              <div className="field">
                <label className="label">Password</label>
                <p className="control">
                  <input className="input" type="password" name="password" value={this.state.password} onChange={this.handlePasswordChange} />
                </p>
              </div>
			  <div className="field">
                <label className="label">Confirm your Password</label>
                <p className="control">
                  <input className="input" type="password" name="passcheck" value={this.state.passcheck} onChange={this.handlePassCheckChange} />
                </p>
              </div>
              <div className="field">
                <p className="control">
                  <input className="button is-primary" type="submit" value="Submit" />
                </p>
              </div>
            </form>
          </div>
        </div>
      </div>
    )
  }
}
