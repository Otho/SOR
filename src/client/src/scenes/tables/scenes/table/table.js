import React from 'react';

import {Route} from "react-router-dom";

import io from 'socket.io-client';
import Auth from 'services/auth';

import "./tables.css";
import {API} from 'config';
let socket = io(API.socket);

export default class Table extends React.Component{
  constructor(props) {
    super(props);

    this.state = {
      messages: [
      ],
      users: [

      ]
    }

  }

  componentDidMount() {
    let table = this;
    socket.on('chat message', function(msg){
      table.setState({messages: [...table.state.messages, msg]});
    });

    socket.on('users', function(users){
      table.setState({users: users});
    });

    socket.emit('chatconnect', {username: Auth.username});
  }

  render () {
    return (
      <div>
      <Route exact path={`${this.props.match.url}/new`} render={props => {
        return (
         <p> New </p>
        )
      }}/>
      <Route path={`${this.props.match.url}/view/:id`} render={props => {
        return(
          <TableView messages={this.state.messages} users={this.state.users} {...props} />
        )
      }}/>
      </div>
    )
  }
}

class TableView extends React.Component {
  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);

    this.state = {
      message: ""
    }
  }

  handleSubmit(event) {
    event.preventDefault();
    socket.emit("chat message", {user: Auth.username, text: this.state.message});
    this.setState({message: "" });
  }

  handleChange(event) {
    this.setState({message: event.target.value });
  }

  render () {
    return (
      <div className="columns">
        <div className="column is-8">
          <div className="chat-messages">
          { this.props.messages && this.props.messages.map(function(message, index) {
            return (
              <div key={index} className="chat-message">
                {message.user}: {message.text}
              </div>
            )
          })}
          </div>
          <form onSubmit={this.handleSubmit}>
            <input onChange={this.handleChange} value={this.state.message}/><button>Send</button>
          </form>
        </div>
        <div className="column is-4">
          Users logged:
          { this.props.users && this.props.users.map(function(user, index) {
            return (
              <div key={index} className="chat-message">
                {user.username}
              </div>
            )
          })}
        </div>
      </div>
    )
  }
}
