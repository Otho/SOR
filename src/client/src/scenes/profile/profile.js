import React from 'react';
import Auth from '../../services/auth';
import {
  Route,
  Link
} from 'react-router-dom'

export default class Profile extends React.Component{

  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();
    console.log("hey");
  }

  render () {
	  return (
      <div>
        <Route exact path={`${this.props.match.url}/`} component={View} {...this.props}/>
        <Route exact path={`${this.props.match.url}/edit`} component={Edit} {...this.props} handleSubmit={this.handleSubmit}/>
      </div>
	  )
  }
}

function View(props) {
  return (
    <div className="columns">
			<div className="column is-4 is-offset-4 has-text-centered">
				<table className="table is-striped">
					<tbody>

						<tr>
							<th>Username</th>
							<td>{Auth.username}</td>
						</tr>

						<tr>
							<th>E-Mail</th>
							<td>{Auth.email}</td>
						</tr>

					</tbody>
				</table>
        <table className="table is-striped">
          <tbody>

            <tr>
              <th>Nickname</th>
              <td>{Auth.nickname}</td>
            </tr>

            <tr>
              <th>Preferred Systems</th>
              <td>{Auth.preferredSystems}</td>
            </tr>

            <tr>
              <th>Preferred Narratives</th>
              <td>{Auth.preferredNarratives}</td>
            </tr>

            <tr>
              <th>Preferred Role</th>
              <td>{Auth.preferredRole}</td>
            </tr>

            <tr>
              <th>Real Name</th>
              <td>{Auth.realName}</td>
            </tr>

            <tr>
              <th>Phone Number</th>
              <td>{Auth.phone}</td>
            </tr>

            <tr>
              <th>Social Media</th>
              <td>{Auth.socialMedia}</td>
            </tr>

          </tbody>
        </table>

        <div className="field">
          <Link to={`${props.match.url}/edit`}>
            <p className="control" style={{textAlign: "center"}}>
              <input className="button is-primary" type="button" value="Edit" />
            </p>
          </Link>
        </div>

			</div>
		</div>
  )
}

function Edit(props) {
  return (
    <div className="columns">
			<div className="column is-4 is-offset-4 has-text-centered">
        <h2 className="title"> Edit </h2>
        <form onSubmit={props.handleSubmit}>
          <table className="table is-striped">
  					<tbody>

  						<tr>
  							<th>Username</th>
  							<td><input className="input" type="text" name="username" defaultValue={Auth.username} /></td>
  						</tr>

  						<tr>
  							<th>E-Mail</th>
  							<td><input className="input" type="text" name="email" defaultValue={Auth.email} /></td>
  						</tr>

  					</tbody>
  				</table>

          <table className="table is-striped">
            <tbody>

              <tr>
                <th>Nickname</th>
                <td><input className="input" type="text" name="nickname" defaultValue={Auth.nickname} /></td>
              </tr>

              <tr>
                <th>Preferred Systems</th>
                <td><input className="input" type="text" name="preferredSystems" defaultValue={Auth.preferredSystems} /></td>
              </tr>

              <tr>
                <th>Preferred Narratives</th>
                <td><input className="input" type="text" name="preferredNarratives" defaultValue={Auth.preferredNarratives} /></td>
              </tr>

              <tr>
                <th>Preferred Role</th>
                <td><input className="input" type="text" name="preferredRole" defaultValue={Auth.preferredRole} /></td>
              </tr>

              <tr>
                <th>Real Name</th>
                <td><input className="input" type="text" name="realName" defaultValue={Auth.realName} /></td>
              </tr>

              <tr>
                <th>Phone Number</th>
                <td><input className="input" type="text" name="phone" defaultValue={Auth.phone} /></td>
              </tr>

              <tr>
                <th>Social Media</th>
                <td><input className="input" type="text" name="socialMedia" defaultValue={Auth.socialMedia} /></td>
              </tr>

            </tbody>
          </table>
          <Link to={`/profile`}>
            <input className="button is-primary" type="submit" value="Submit" />
          </Link>
        </form>
      </div>
    </div>
  )
}
