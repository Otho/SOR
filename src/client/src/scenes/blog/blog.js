import React from 'react';
import Posts from '../../services/posts';
import Auth from '../../services/auth';

import Post from 'components/post/post';
import PostContainer from 'components/post_container/post_container';

import PostCommentContainer from 'components/post_comment_container/post_comment_container';
import PostComment from 'components/post_comment/post_comment';

import {
  Route,
  Redirect
} from 'react-router-dom'

import './blog.css';

export default class Blog extends React.Component{

  constructor(props) {
    super(props);

    this.state = {
      posts: Posts.posts,
      post: null,
      comment: ''
    };
    this.handleTextChange = this.handleTextChange.bind(this);
  }

  componentDidMount() {
    Posts.fetch((posts) => {
      this.setState({posts: posts});
    })
  }

  handleTextChange(event) {
    this.setState({comment: event.target.value});
  }


  render () {
    return (
      <div>
        <Route exact path={`${this.props.match.url}/`} render={props =>
          <PostContainer>
            <div>
              {
                this.state.posts && this.state.posts.map(function(post, index) {
                  return (<Post key={index} post={post} />)
                })
              }
            </div>
          </PostContainer>
        }/>
        <Route path={`${this.props.match.url}/:perma_link`} render={props => {
          let perma_link = props.match.params.perma_link;
          let post = Posts.getPost(perma_link);
          function handleCommentCreate(event) {
            event.preventDefault();
            event.target.reset();
            let comment = {username: Auth.username, perma_link: perma_link, text: this.state.comment};
            Posts.createComment(comment, function() {
              post.comments.splice(0, 0, {author:{username: Auth.username}, ...comment});
              this.setState({posts: this.state.posts})
            }, this);
          }

          if(post) {
            return (
              <PostContainer {...props}>
                <Post post={post} />
                <CommentCreator handleCreate={handleCommentCreate.bind(this)} text={this.state.text} handleTextChange={this.handleTextChange}/>
                <br/>
                Comments:
                <PostCommentContainer>
                  {
                    post.comments && post.comments.map(function(comment, index) {
                      return (<PostComment key={index} _comment={comment} />)
                    })
                  }
                </PostCommentContainer>
              </PostContainer>
            )
          } else {
            return (
              <Redirect to="/blog"/>
            )
          }
        }}/>
      </div>
    )
  }
}

function CommentCreator(props) {
  if (!Auth.isAuthenticated) return null;
  return (
    <form onSubmit={props.handleCreate} className="has-text-left">
      <div className="field">
        <label className="label">{Auth.username +':'}</label>
        <p className="control">
          <input className="input" type="text" name="text" placeholder="Bla bla bla" value={props.text} onChange={props.handleTextChange} />
        </p>
      </div>
      <div className="field">
        <p className="control">
          <input className="button is-primary" type="submit" value="Submit" />
        </p>
      </div>
    </form>
  )
}
