import React from 'react';
import {
  Route
} from 'react-router-dom'

function SplitRoute ({ condition, componentTrue, componentFalse, ...rest }) {
  console.log(condition());
  return (
    condition() ?
    <Route {...rest} component={componentTrue}/>
    :
    <Route {...rest} component={componentFalse}/>
  )
}

export default SplitRoute;
