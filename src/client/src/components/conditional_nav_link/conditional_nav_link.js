import React from 'react';
import {
  NavLink
} from 'react-router-dom'

const ConditionalNavLink = ({ component: Component, condition, ...rest }) => {
  return (
    condition ?
    // <NavLink {...rest} render={props => (<Component {...props}/>)}/>
    <NavLink {...rest} />
    : null
  )
}

export default ConditionalNavLink;
